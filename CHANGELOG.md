# Changelog

#### v1.1.3

- Chore: accept base 3.0.
- Chore: update license list data to 3.24.

#### v1.1.2

- Chore: accept json 2 and 3.

#### v1.1.1

- Chore: remove use dependency on containers, system, and text.

### v1.1

- Chore: switch to base 2.0; drop clean-platform dependency.
- Chore: update license list data to 3.21.

#### v1.0.1

- Chore: move to https://clean-lang.org.
- Chore: update license list data.

## v1.0.0

First tagged version.
