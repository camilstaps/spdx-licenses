# SPDX Licenses

This is a [Clean][] library for the [SPDX License List][]. The library is
generated from https://github.com/spdx/license-list-data.

It is published on the Nitrile registry as [`spdx-licenses`][registry-page].

This library is created and maintained by [Camil Staps][].

[![](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, the person who associated CC0 with this work
has waived all copyright and related or neighboring rights to this work. This
work is published from The Netherlands.

[Camil Staps]: https://camilstaps.nl/
[Clean]: https://clean-lang.org/
[registry-page]: https://clean-lang.org/pkg/spdx-licenses/
[SPDX License List]: https://spdx.org/licenses/
